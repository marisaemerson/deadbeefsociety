#include <stdio.h>
#include <string.h>

#define MAXSIZE 255

/*
 * Warmup: stack overflow with a twist.
 *
 * GDB can print values according to its data type
 * https://sourceware.org/gdb/onlinedocs/gdb/Output-Formats.html
 */

void
vuln(char *s, int len)
{
	char buf[MAXSIZE];
	int max = MAXSIZE;

	if (len > max)
		errx(1, "Too big!\n");

	strcpy(buf, s);
}

int
main(int argc, char *argv[])
{
	char buf[64*1024];
	int n, l;

	if (argc < 3)
		errx(1, "Argument required\n");

	n = atoi(argv[2]);
	l = strlen(argv[1]);

	if (l < n)
		n = l;

	vuln(argv[1], n);
}

