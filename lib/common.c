#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
print_flag(char *level)
{
	char flag_path[50];
	char password[50];
	FILE *f;

	strncpy(flag_path, level, sizeof(flag_path));
	f = fopen(flag_path, "r");
	if (!f) {
		fprintf(stderr, "Error opening file\n");
		exit(1);
	}

	fgets(password, sizeof(password), f);
	printf("Winner! Flag: %s\n", password);
}

